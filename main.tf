data "aws_security_group" "default" {
  name   = "default"
  vpc_id = var.vpc_id
}

resource "aws_ses_domain_identity" "ses_domain" {
  domain = var.domain
}

resource "aws_route53_record" "ses_verification_record" {
  count = var.aws_r53 == true ? 1 : 0

  name    = "_amazonses.${var.domain}"
  records = [aws_ses_domain_identity.ses_domain.verification_token]
  ttl     = "3600"
  type    = "TXT"
  zone_id = var.zone_id
}

resource "aws_ses_domain_identity_verification" "domain_verification" {
  count = var.aws_r53 == true ? 1 : 0

  depends_on = [aws_route53_record.ses_verification_record]
  domain     = aws_ses_domain_identity.ses_domain.id
}

resource "aws_ses_domain_dkim" "ses_domain_dkim" {
  domain = var.domain
}

resource "aws_route53_record" "ses_dkim_record" {
  count = var.aws_r53 == true ? 3 : 0

  name    = "${element(aws_ses_domain_dkim.ses_domain_dkim.dkim_tokens, count.index)}._domainkey.${var.domain}"
  records = ["${element(aws_ses_domain_dkim.ses_domain_dkim.dkim_tokens, count.index)}.dkim.amazonses.com"]
  ttl     = "3600"
  type    = "CNAME"
  zone_id = var.zone_id
}

resource "aws_vpc_endpoint" "ses_endpoint" {
  count = var.aws_vpc_endpoint == true ? 1 : 0

  private_dns_enabled = true
  security_group_ids  = [data.aws_security_group.default.id]
  service_name        = "com.amazonaws.eu-west-1.email-smtp"
  vpc_endpoint_type   = "Interface"
  vpc_id              = var.vpc_id

  tags = var.default_tags
}
