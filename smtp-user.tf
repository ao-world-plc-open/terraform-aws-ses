resource "aws_iam_user" "smtp_user" {
  name = "SES-SMTP-Credentials"
  tags = var.default_tags
}

resource "aws_iam_access_key" "smtp_access_key" {
  user = aws_iam_user.smtp_user.name
}

data "aws_iam_policy_document" "ses_send_access" {
  statement {
    actions = [
      "ses:SendRawEmail",
    ]
    effect    = "Allow"
    resources = ["*"]
  }
}

resource "aws_iam_user_policy" "ses" {
  policy = data.aws_iam_policy_document.ses_send_access.json
  user   = aws_iam_user.smtp_user.name
}
