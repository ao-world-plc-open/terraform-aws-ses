output "dkim_tokens" {
  description = "DKIM Tokens created from SES DKIM"
  value       = aws_ses_domain_dkim.ses_domain_dkim.dkim_tokens
}

output "identity_verification_arn" {
  description = "The Domain ARN of the Domain Identity"
  value       = aws_ses_domain_identity_verification.domain_verification.*.arn
}

output "identity_verification_id" {
  description = "The Domain Name of the Domain Identity"
  value       = aws_ses_domain_identity_verification.domain_verification.*.id
}

output "r53_dkim_record_fdqn" {
  description = "R53 DKIM Record FQDN"
  value       = aws_route53_record.ses_dkim_record.*.fqdn
}

output "r53_dkim_record_name" {
  description = "R53 DKIM Record Name"
  value       = aws_route53_record.ses_dkim_record.*.name
}

output "r53_verification_record_fdqn" {
  description = "R53 Verification Record FQDN"
  value       = aws_route53_record.ses_verification_record.*.fqdn
}

output "r53_verification_record_name" {
  description = "R53 Verification Record Name"
  value       = aws_route53_record.ses_verification_record.*.name
}

output "ses_domain_arn" {
  description = "SES Domain ARN"
  value       = aws_ses_domain_identity.ses_domain.arn
}

output "ses_domain_verification_token" {
  description = "SES Domain Verification Token"
  value       = aws_ses_domain_identity.ses_domain.verification_token
}

output "ses_endpoint_arn" {
  description = "ARN for SES VPC Endpoint"
  value       = aws_vpc_endpoint.ses_endpoint.*.arn
}

output "ses_endpoint_dns_entry" {
  description = "DNS Entry for SES VPC Endpoint"
  value       = aws_vpc_endpoint.ses_endpoint.*.dns_entry
}

output "ses_endpoint_id" {
  description = "ID for SES VPC Endpoint"
  value       = aws_vpc_endpoint.ses_endpoint.*.id
}

output "ses_smtp_access_key" {
  description = "SES User Access Key"
  value       = aws_iam_access_key.smtp_access_key.id
}

output "ses_smtp_arn" {
  description = "SES User ARN"
  value       = aws_iam_user.smtp_user.arn
}

output "ses_smtp_username" {
  description = "SES SMTP Username"
  value       = aws_iam_user.smtp_user.name
}

output "ses_smtp_pass" {
  description = "SES User Password"
  value       = aws_iam_access_key.smtp_access_key.ses_smtp_password_v4
}
