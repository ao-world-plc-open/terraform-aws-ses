# Change Log

## v1.1.0 (20/12/2021)

* Added .terraform-doc.yml to auto generate (mostly) README.md
* Added basic example config for README.md
* All variables and outputs have descriptions and types, and have been
  organised alphabetically
* Ran `terraform fmt` on the repository
* Updated CONTRIBUTING.md to cover running fmt and ensuring docs are
  regenerated
* Added .gitlab-ci.yml to do basic checks for fmt and out of date docs

## v1.0.0 (01/06/2021)

* Added minimum provider version constraints per Terraform best practice
* Confirmed terraform validate passing for terraform 0.13, 0.14, and 0.15
* Minimum aws provider version set to where support for .aws/sso/cache is enabled
* Terraform 0.13 now the minimum version due to use of required_providers syntax

## v0.2 (20/01/2021)

* Added Coniditonal Variable
* If the servers utilising the service are within your aws account this will create a VPC endpoint

## v0.1 (20/01/2021)

* Initial Commit
