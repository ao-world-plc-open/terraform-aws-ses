variable "aws_r53" {
  description = "Is your DNS managed via Route 53? Do you need the associated DKIM records creating and attaching to your hosted zone?"
  type        = bool
  default     = false
}

variable "aws_vpc_endpoint" {
  description = "Are the servers utilising the service and sending email via SES within your AWS account?"
  type        = bool
  default     = false
}
variable "default_tags" {
  description = "Default tags to assgin the module resources"
  type        = map(string)
  default     = {}
}

variable "domain" {
  description = "The Domain Name we are sending emails from with the SES service"
  type        = string
}

variable "vpc_id" {
  description = "The VPC ID. Required if `aws_vpc_endpoint` is enabled"
  type        = string
  default     = ""
}

variable "zone_id" {
  description = "The R53 Zone ID. Rquired if `aws_r53` is enabled"
  type        = string
  default     = ""
}
