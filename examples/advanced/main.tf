module "ses-test" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-ses.git?ref=v1.1.0"

  aws_r53          = false
  aws_vpc_endpoint = true
  domain           = "example.com"
  vpc_id           = "vpc-id"
}