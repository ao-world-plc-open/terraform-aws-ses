# Terraform Module: AWS SES

Module for the creation of the Simple Email Service. This module contains the
required code for a Route 53 managed DNS and a non Route 53 managed DNS. 

* [Example Usage](#example-usage)
  * [Basic](#basic)
  * [Advanced](#advanced)
* [Requirements](#requirements)
* [Inputs](#inputs)
* [Outputs](#outputs)
* [Contributing](#contributing)
* [Change Log](#change-log)

## Example Usage

### Basic

This basic example will deploy SES into your account, verify it, and add DKIM
records to your domain. The SES endpoint will be through a VPC endpoint,
which is only accessible from your account:

```hcl
module "ses-test" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-ses.git?ref=v1.1.0"

  aws_r53          = true
  aws_vpc_endpoint = true
  domain           = "team.example.com"
  vpc_id           = "vpc-id"
  zone_id          = "zone-id"
}
```

### Advanced

This example deploys SES into your account with a VPC endpoint, however for a
domain you wish to send email from that you don't directly control. The owner
of the domain will need to add the validation and DKIM records this module
outputs:

```hcl
module "ses-test" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-ses.git?ref=v1.1.0"

  aws_r53          = false
  aws_vpc_endpoint = true
  domain           = "example.com"
  vpc_id           = "vpc-id"
}
```

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | >= 3.26.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| domain | The Domain Name we are sending emails from with the SES service | `string` | n/a | yes |
| aws\_r53 | Is your DNS managed via Route 53? Do you need the associated DKIM records creating and attaching to your hosted zone? | `bool` | `false` | no |
| aws\_vpc\_endpoint | Are the servers utilising the service and sending email via SES within your AWS account? | `bool` | `false` | no |
| default\_tags | Default tags to assgin the module resources | `map(string)` | `{}` | no |
| vpc\_id | The VPC ID. Required if `aws_vpc_endpoint` is enabled | `string` | `""` | no |
| zone\_id | The R53 Zone ID. Rquired if `aws_r53` is enabled | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| dkim\_tokens | DKIM Tokens created from SES DKIM |
| identity\_verification\_arn | The Domain ARN of the Domain Identity |
| identity\_verification\_id | The Domain Name of the Domain Identity |
| r53\_dkim\_record\_fdqn | R53 DKIM Record FQDN |
| r53\_dkim\_record\_name | R53 DKIM Record Name |
| r53\_verification\_record\_fdqn | R53 Verification Record FQDN |
| r53\_verification\_record\_name | R53 Verification Record Name |
| ses\_domain\_arn | SES Domain ARN |
| ses\_domain\_verification\_token | SES Domain Verification Token |
| ses\_endpoint\_arn | ARN for SES VPC Endpoint |
| ses\_endpoint\_dns\_entry | DNS Entry for SES VPC Endpoint |
| ses\_endpoint\_id | ID for SES VPC Endpoint |
| ses\_smtp\_access\_key | SES User Access Key |
| ses\_smtp\_arn | SES User ARN |
| ses\_smtp\_pass | SES User Password |
| ses\_smtp\_username | SES SMTP Username |

## Contributing

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to contribute to changes to this module.

## Change Log

Please see [CHANGELOG.md](CHANGELOG.md) for changes made between different tag versions of the module.